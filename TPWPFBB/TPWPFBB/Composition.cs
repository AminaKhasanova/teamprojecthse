﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB
{
    public class Composition : Operator
    {
        // f(x) = a(b(x)), f(x) = (a . b) (x)
        private Composition(Function a, Function b) : base(a, b) { }

        public static Function New(Function a, Function b)
        {
            if (b is Sample_Func)
                return a;
            if (a is Sample_Func)
                return b;
            if (a is Constant)
                return a;
            if (b is Constant)
                return b;

            return new Composition(a, b);
        }

        public override Function Derivative()
        {
            return (leftFunc[1] % rightFunc) * rightFunc[1];
        }

        public override string ToString()
        {
            return "(" + leftFunc + " ~ " + rightFunc + ")";
        }
    }
}
