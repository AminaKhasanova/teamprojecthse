﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB
{
    public abstract class Function
    {
        public abstract Function Derivative();

        public Function this[int n] // n - степень дифференцирования
        {
            get
            {
                if (n < 0)
                    throw new ArgumentException("n must be more than 0");
                var f = this;
                for (int i = 0; i < n; ++i)
                    f = f.Derivative();
                return f;
            }
        }

        public static Function operator +(Function a, Function b)
        {
            return new Addition(a, b);
        }

        public static Function operator +(double k, Function b)
        {
            return new Constant(k) + b;
        }

        public static Function operator +(Function a, double k)
        {
            return a + new Constant(k);
        }

        public static Function operator /(Function c, Function d)
        {
            return new Division(d, c);
        }
        public static Function operator /(double k, Function b)
        {
            return new Constant(k) / b;
        }

        public static Function operator /(Function a, double k)
        {
            return a / new Constant(k);
        }

        public static Function operator -(Function a, Function b)
        {
            return new Minus(a, b);
        }

        public static Function operator -(double k, Function b)
        {
            return new Constant(k) - b;
        }

        public static Function operator -(Function a, double k)
        {
            return a - new Constant(k);
        }

        public static Function operator *(Function a, Function b)
        {
            return new Multiplication(a, b);
        }

        public static Function operator *(double k, Function b)
        {
            return new Constant(k) * b;
        }

        public static Function operator *(Function a, double k)
        {
            return a * new Constant(k);
        }
        public static Function operator %(Function a, Function b)
        {
            return Composition.New(a, b);
        }
    }
}
