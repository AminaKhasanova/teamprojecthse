﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB //IDENTITY
{
    class Sample_Func : Function // line func realisation  f(x) = x
    {
        public override Function Derivative()
        {
            return Prim_Func.Zero + 1;
        }

        public override string ToString()
        {
            return "x";
        }
    }
}
