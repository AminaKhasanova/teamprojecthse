﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB
{
    public class Cos : Function
    {
        public override Function Derivative()
        {
            return -1 * Prim_Func.Sin;
        }

        public override string ToString()
        {
            return "cos x";
            //test
        }
    }
}
