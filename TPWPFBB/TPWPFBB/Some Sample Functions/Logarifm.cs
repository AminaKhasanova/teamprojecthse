﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB
{
    public class Logarifm : Function
    {
        public Logarifm(double a = Math.E)
        {
            this.a = a;
        }

        public override Function Derivative()
        {
            return 1 / (Prim_Func.Sf * Math.Log(a, Math.E));
        }

        public override string ToString()
        {
            if (Math.Abs(a - Math.E) <= 10e-6)
                return "ln x";
            return "log[a](x)";
        }

        private double a;
    }
}
