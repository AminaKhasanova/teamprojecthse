﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWPFBB
{
    public class Sin : Function
    {
        public override Function Derivative()
        {
            return Prim_Func.Cos;
        }

        public override string ToString()
        {
            return "sin x";
        }
    }
}
