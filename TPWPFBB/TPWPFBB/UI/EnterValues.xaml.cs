﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TPWPFBB
{
    /// <summary>
    /// Логика взаимодействия для EnterValues.xaml
    /// </summary>
    public partial class EnterValues : Window
    {
        public EnterValues()
        {
            InitializeComponent();
        }
        private void MainSubmitionButton_Click(object sender, RoutedEventArgs e)
        {
            var answer = new Answer();
            answer.Show();
            Close();
        }

        //EnterDegree
        public void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int degree = int.Parse(Degree.Text);
            }
            catch
            {
                var enterValuesException = new EnterValuesException();
                enterValuesException.Show();
                Close();
            }
        }

        //EnterFunction
        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            string func = Func.Text;

            List<string> variablesminus = new List<string>();
            string[] splitminus = func.Split(new Char[] { '-' });
            foreach (string s in splitminus)
            {
                variablesminus.Add(s);
            }

            for (int i = 0; i < variablesminus.Count();i++)
            {
                if (variablesminus[i] == "sin")
                {
                    variablesminus[i] = Prim_Func.Sin();
                };
                if (variablesminus[i] == "cos")
                {
                    variablesminus[i] = Prim_Func.Cos();
                };
                if (variablesminus[i] == "log")
                {
                    variablesminus[i] = Prim_Func.Ln();
                };



            }

            //string vp1 = variablesminus[0];
            //string vp2 = variablesminus[1];
            //Type variablesminus[0] = this.GetType();
            //{

            //}

            //List<string> variablesplus = new List<string>();
            //string[] splitplus = func.Split(new Char[] { '+' });
            //foreach (string s in splitplus)
            //{
            //    variablesplus.Add(s);
            //}

            //List<string> variables2 = new List<string>();
            //foreach (string s in variables)
            //{ }
            //string[] split2 = func.Split(new Char[] { '/', '*', '^' });
            //foreach (string s in split2)
            //{
            //  variables2.Add(s);
            //}

        }
    }
}
