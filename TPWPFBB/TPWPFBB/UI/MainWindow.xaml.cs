﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TPWPFBB;

namespace TPWPFBB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonBegin_Click(object sender, RoutedEventArgs e)
        {
            var enterValues = new EnterValues();
            enterValues.Show();
            Close();
        }

        private void ButtonSpreadsheet_Click(object sender, RoutedEventArgs e)
        {
            var spreadsheet = new SpreadSheet();
            spreadsheet.Show();
            Close();
        }
    }
}
