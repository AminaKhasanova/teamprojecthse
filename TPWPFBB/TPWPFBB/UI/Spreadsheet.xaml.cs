﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Resources;

namespace TPWPFBB
{
    /// <summary>
    /// Логика взаимодействия для SpreadSheet.xaml
    /// </summary>
    public partial class SpreadSheet : Window
    {
        public SpreadSheet()
        {
            InitializeComponent();
        }

        private void BackToMainWindow_Button(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Close();

        }
    }
}
